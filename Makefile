AS = nasm
ASFLAGS = -f bin
COMMON_INCLUDE_DIR = ./include

STAGE1_SRC_DIR = ./SysBoot/Stage1
_STAGE1_OBJ = Stage1.bin
STAGE1_OBJ = $(patsubst %,$(STAGE1_SRC_DIR)/%,$(_STAGE1_OBJ))

STAGE2_SRC_DIR = ./SysBoot/Stage2
_STAGE2_OBJ = Stage2.sys
STAGE2_OBJ = $(patsubst %,$(STAGE2_SRC_DIR)/%,$(_STAGE2_OBJ))

SYSCORE_INCLUDE_DIR = ./SysCore/Include
KERNEL_SRC_DIR = ./SysCore/Kernel
_KERNEL_OBJ = Krnl.sys
KERNEL_OBJ = $(patsubst %,$(KERNEL_SRC_DIR)/%,$(_KERNEL_OBJ))

image: $(STAGE1_OBJ) $(STAGE2_OBJ) $(KERNEL_OBJ)
	sh ./create_os_image.sh

run: image
	bochs -f ./bochs/bochs_config -q

.PHONY: clean

clean:
	rm -f $(STAGE1_SRC_DIR)/*.bin
	rm -f $(STAGE2_SRC_DIR)/*.sys	
	rm -f $(KERNEL_SRC_DIR)/*.sys
	rm -f ./floppy.img

$(STAGE1_SRC_DIR)/%.bin: $(STAGE1_SRC_DIR)/%.asm
	$(AS) $(AS_FLAGS) $< -o $@
	
$(STAGE2_SRC_DIR)/%.sys: $(STAGE2_SRC_DIR)/%.asm
	$(AS) -i $(STAGE2_SRC_DIR) $(AS_FLAGS) $< -o $@

$(KERNEL_SRC_DIR)/%.sys: $(KERNEL_SRC_DIR)/%.asm
	$(AS) -i $(SYSCORE_INCLUDE_DIR) $(AS_FLAGS) $< -o $@
