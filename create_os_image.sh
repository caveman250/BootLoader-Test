base_dir=$(dirname "$0")
echo "$base_dir"	
	
sudo umount $base_dir/floppy.img

#delete old floppy
sudo rm $base_dir/floppy.img
sudo rm -R $base_dir/floppy_mnt_dir

#create new floppy image
sudo mkfs.msdos -F 12 -C $base_dir/floppy.img 1440
sudo mkdir $base_dir/floppy_mnt_dir
sudo mount $base_dir/floppy.img $base_dir/floppy_mnt_dir -t msdos -o loop,fat=12

sudo cp $base_dir/SysBoot/Stage2/Stage2.sys $base_dir/floppy_mnt_dir
sudo cp $base_dir/SysCore/Kernel/Krnl.sys $base_dir/floppy_mnt_dir
sudo umount $base_dir/floppy.img

#copy binary files to floppy image
sudo dd if=$base_dir/SysBoot/Stage1/Stage1.bin bs=512 of=$base_dir/floppy.img conv=notrunc count=1
