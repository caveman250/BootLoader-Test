;*********************************************
;	Boot1.asm
;		- MyOS Bootloader
;*********************************************

bits	16						; we are in 16 bit real mode

org	0						; we will set regisers later

start: jmp main				; jump to start of bootloader

;*********************************************
;	BIOS Parameter Block
;*********************************************

; BPB Begins 3 bytes from start. We do a far jump, which is 3 bytes in size.
; If you use a short jump, add a "nop" after it to offset the 3rd byte.

bpbOEM			     DB "My OS   "			; OEM identifier (Cannot exceed 8 bytes!)
bpbBytesPerSector:  	DW 512
bpbSectorsPerCluster: 	DB 1
bpbReservedSectors: 	DW 1
bpbNumberOfFATs: 	     DB 2
bpbRootEntries: 	     DW 224
bpbTotalSectors: 	     DW 2880
bpbMedia: 		     DB 0xf8
bpbSectorsPerFAT: 	     DW 9
bpbSectorsPerTrack: 	DW 18
bpbHeadsPerCylinder: 	DW 2
bpbHiddenSectors: 	     DD 0
bpbTotalSectorsBig:      DD 0
bsDriveNumber: 	     DB 0
bsUnused: 		     DB 0
bsExtBootSignature: 	DB 0x29
bsSerialNumber:	     DD 0xa0a1a2a3
bsVolumeLabel: 	     DB "MOS FLOPPY "
bsFileSystem: 	          DB "FAT12   "

PrintS16:
		pusha				; save registers
.PrintS16Loop:
		lodsb				; load next byte from string from SI to AL
		or	al, al			; Does AL=0?
		jz	PrintS16Done		; Yep, null terminator found-bail out
		mov	ah, 0eh			; Nope-Print the character
		int	10h			    ; invoke BIOS
		jmp	.PrintS16Loop	; Repeat until null terminator found
PrintS16Done:
		popa				; restore registers
		ret				    ; we are done, so return

;************************************************;
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:BX=>Buffer to read to
;************************************************;

ReadSectors:
     .MAIN:
          mov       di, 0x0005                          ; set di to 5, we will decrement this every time a read fails.
     .SECTORLOOP:
          push      ax                                  ; push registers so that we can restore their current value with pop later.  
          push      bx
          push      cx
          call      ConvertLBAToCHS                     ; convert starting sector to CHS
          mov       ah, 0x02                            ; moving 0x02 into ah tells the bios that we want to read sectors when performing a 0x13 interupt.
          mov       al, 0x01                            ; during a 0x13 interupt al is the number of sectors to read.
          mov       ch, BYTE [absoluteTrack]            ; store the track in ch register 
          mov       cl, BYTE [absoluteSector]           ; store the sector in cl register 
          mov       dh, BYTE [absoluteHead]             ; store the head in dh register 
          mov       dl, BYTE [bsDriveNumber]            ; store the drive number in dl register 
          int       0x13                                ; 0x13 BIOS interupt.
          jnc       .SUCCESS                            ; if the carry flag is not set, jump to .SUCCESS, the carry flag gets set by int 0x13 if there is an error
          xor       ax, ax                              ; BIOS reset disk
          int       0x13                                ; 0x13 BIOS interupt.
          dec       di                                  ; decrement error counter, if di == 0 then the zero flag will be set, otherwise it will be cleared.
          pop       cx                                  ; restore registers to previous values.
          pop       bx
          pop       ax
          jnz       .SECTORLOOP                         ; if the zero flag is not set, jump back to .SECTORLOOP
          int       0x18                                ; BIOS interupt 0x18. throw an error that there is no bootable disk, apparently this is inconsistent accross processors?
     .SUCCESS:
          mov       si, msgProgress                     ; move the progress message into si register
          call      PrintS16                            ; call our print method
          pop       cx                                  ; restore cx, bx, ax to their previous values.
          pop       bx
          pop       ax
          add       bx, WORD [bpbBytesPerSector]        ; add bpbBytesPerSector to bx. bx is the buffer address.
          inc       ax                                  ; queue next sector (div uses the value stored in ax, see ConvertLBAToCHS: for where this is used.)
          loop      .MAIN                               ; read next sector
          ret

;************************************************;
; Convert Logical Block Addressing (LBA) to Cylinder/Head/Sector (CHS) addressing
; AX=>LBA Address to convert
;
; absolute sector = (logical sector % sectors per track) + 1
; absolute head   = (logical sector / sectors per track) % number of heads
; absolute track  = logical sector / (sectors per track * number of heads)
;
;************************************************;

ConvertLBAToCHS:
          xor       dx, dx                              ; clear dx (xor sets the left value to 1 if the right value is different, since dx == dx it will set dx to 0)
          div       WORD [bpbSectorsPerTrack]           ; ax / WORD [bpbSectorsPerTrack]. because the divisor is a word the quotiant will be stored in ax with the remainder in dx
          inc       dl                                  ; add 1 to the remainder (why is this dl instead of dx, does that even change the behaiviour?)
          mov       BYTE [absoluteSector], dl           ; absoluteSector = mod (remainder) of above equation + 1
          xor       dx, dx                              ; clear dx
          div       WORD [bpbHeadsPerCylinder]          ; (logical sector / sectors per track) / WORD [bpbHeadsPerCylinder]  
          mov       BYTE [absoluteHead], dl             ; absoluteHead = mod of above equation
          mov       BYTE [absoluteTrack], al            ; absoluteTrack = quotiant of above equation (x / y / z == x / (y * z))
          ret

;************************************************;
; Convert CHS to LBA
; LBA = (cluster - 2) * sectors per cluster
;************************************************;

ConvertCHSToLBA:
          sub       ax, 0x0002                          ; subract 2 from cluster number
          xor       cx, cx                              ; clear cx
          mov       cl, BYTE [bpbSectorsPerCluster]     ; convert byte to word (place bpbSectorsPerCluster in lower half of cx register)
          mul       cx                                  ; multiply ax with cx, because cx is 16 bit the result will be stored in dx (left most) and ax (right most)
          add       ax, WORD [datasector]               ; offset by base data sector.
          ret
     

;*********************************************
;	Bootloader Entry Point
;*********************************************

main:
     ;----------------------------------------------------
     ; code located at 0000:7C00, adjust segment registers
     ;----------------------------------------------------
     
          cli						       ; disable interrupts
          mov       ax, 0x07C0			  ; setup registers to point to our segment
          mov       ds, ax                     ; ds = data segment
          mov       es, ax                     ; es = extra segment
          mov       fs, ax                     ; fs = another extra segment
          mov       gs, ax                     ; gs = another extra segment

     ;----------------------------------------------------
     ; create stack
     ;----------------------------------------------------
     
          mov       ax, 0x0000			  ; stack starting address
          mov       ss, ax                     ; ss is the starting address of the stack
          mov       sp, 0xFFFF                 ; sp is the top of the stack
          sti						       ; restore interrupts

     ;----------------------------------------------------
     ; Display loading message
     ;----------------------------------------------------
     
          mov       si, msgLoading             ; si is the start address of a 16 bit string
          call      PrintS16                   ; call our print method
          
     ;----------------------------------------------------
     ; Load root directory table
     ;----------------------------------------------------

     LOAD_ROOT:
     
     ; compute size of root directory and store in "cx"
     
          xor       cx, cx                               ; clear cx and dx (dx is used for mul and div results)
          xor       dx, dx
          mov       ax, 0x0020                           ; 32 byte directory entry
          mul       WORD [bpbRootEntries]                ; ax = total size of directory = ax * bpbRootEntries
          div       WORD [bpbBytesPerSector]             ; ax = sectors used by directory = ax / bpbBytesPerSector
          xchg      ax, cx                               ; swap ax and cx (couldnt this just be mov?)
          
     ; compute location of root directory and store in "ax" also compute locatgion of data sector
     
          mov       al, BYTE [bpbNumberOfFATs]            ; store number of FATs in al
          mul       WORD [bpbSectorsPerFAT]               ; ax = sectors used by FATs = number of FATs / sectors per FAT
          add       ax, WORD [bpbReservedSectors]         ; ax = sectors used by FATs + reserved sectors (skip the bootloader)
          mov       WORD [datasector], ax                 ; datasector = ax
          add       WORD [datasector], cx                 ; datasector = location of root directory + size of root directory
          
     ; read root directory into memory (7C00:0200)
     
          mov       bx, 0x0200                            ; copy root dir above bootcode
          call      ReadSectors

     ;----------------------------------------------------
     ; Find stage 2
     ;----------------------------------------------------

     ; browse root directory for binary image
          mov       cx, WORD [bpbRootEntries]             ; loop count = bpbRootEntries
          mov       di, 0x0200                            ; address of first root entry.
     .LOOP:
          push      cx                                    ; push cx so we can restore the loop count
          mov       cx, 0x000B                            ; image name length
          mov       si, ImageName                         ; image name to find
          push      di                                    ; cmpdb will leave di pointing at the address where di and si differ, push di so we can restore the original address afterwards
     rep  cmpsb                                           ; loop while cx > 0 and compare the current character in si and di
          pop       di                                    ; pop di, get original address back.
          je        LOAD_FAT                              ; if the string matches then load the file
          pop       cx                                    ; otherwise, restore loop counter
          add       di, 0x0020                            ; queue next directory entry
          loop      .LOOP                                 ; if cx > 0 jump back to .LOOP
          jmp       FAILURE                               ; loop ended, fail.

     ;----------------------------------------------------
     ; Load FAT
     ;----------------------------------------------------

     LOAD_FAT:
     
     ; save starting cluster of boot image
     
          mov       si, msgCRLF                         ; print a line break
          call      PrintS16
          mov       dx, WORD [di + 0x001A]
          mov       WORD [cluster], dx                  ; file's first cluster
          
     ; compute size of FAT and store in "cx"
     
          xor       ax, ax
          mov       al, BYTE [bpbNumberOfFATs]          ; number of FATs
          mul       WORD [bpbSectorsPerFAT]             ; sectors used by FATs
          mov       cx, ax

     ; compute location of FAT and store in "ax"

          mov       ax, WORD [bpbReservedSectors]       ; adjust for bootsector
          
     ; read FAT into memory (7C00:0200)

          mov       bx, 0x0200                          ; copy FAT above bootcode
          call      ReadSectors

     ; read image file into memory (0050:0000)
     
          mov       si, msgCRLF                         ; print a line break
          call      PrintS16
          mov       ax, 0x0050
          mov       es, ax                              ; destination for image
          mov       bx, 0x0000                          ; destination for image
          push      bx

     ;----------------------------------------------------
     ; Load Stage 2
     ;----------------------------------------------------

     LOAD_IMAGE:
     
          mov       ax, WORD [cluster]                  ; cluster to read
          pop       bx                                  ; buffer to read into
          call      ConvertCHSToLBA                     ; convert cluster to LBA
          xor       cx, cx
          mov       cl, BYTE [bpbSectorsPerCluster]     ; sectors to read
          call      ReadSectors
          push      bx
          
     ; compute next cluster
     
          mov       ax, WORD [cluster]                  ; identify current cluster
          mov       cx, ax                              ; copy current cluster
          mov       dx, ax                              ; copy current cluster
          shr       dx, 0x0001                          ; divide by two
          add       cx, dx                              ; sum for (3/2)
          mov       bx, 0x0200                          ; location of FAT in memory
          add       bx, cx                              ; index into FAT
          mov       dx, WORD [bx]                       ; read two bytes from FAT
          test      ax, 0x0001
          jnz       .ODD_CLUSTER
          
     .EVEN_CLUSTER:
     
          and       dx, 0000111111111111b               ; take low twelve bits
         jmp        .DONE
         
     .ODD_CLUSTER:
     
          shr       dx, 0x0004                          ; take high twelve bits
          
     .DONE:
     
          mov       WORD [cluster], dx                  ; store new cluster
          cmp       dx, 0x0FF0                          ; test for end of file
          jb        LOAD_IMAGE                          ; if dx < 0x0FF0 jump to LOAD_IMAGE
          
     DONE:
     
          mov       si, msgCRLF                         ; print a line break
          call      PrintS16
          push      WORD 0x0050
          push      WORD 0x0000
          retf                                          ; stage 1 complete
          
     FAILURE:
     
          mov       si, msgFailure
          call      PrintS16
          mov       ah, 0x00
          int       0x16                                ; await keypress
          int       0x19                                ; warm boot computer
     
     absoluteSector db 0x00
     absoluteHead   db 0x00
     absoluteTrack  db 0x00
     
     datasector  dw 0x0000
     cluster     dw 0x0000
     ImageName   db "STAGE2  SYS"
     msgLoading  db 0x0D, 0x0A, "Loading Boot Image ", 0x0D, 0x0A, 0x00
     msgCRLF     db 0x0D, 0x0A, 0x00
     msgProgress db ".", 0x00
     msgFailure  db 0x0D, 0x0A, "ERROR : Press Any Key to Reboot", 0x0A, 0x00
     
     TIMES 510-($-$$) DB 0
     DW 0xAA55
