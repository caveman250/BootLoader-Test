;*************************************************
;	stdio.inc
;		-Input/Output methods
;*************************************************
 
%ifndef __STDIO_INC__
%define __STDIO_INC__
 
;==========================================================
;
;	 16 Bit Real Mode Methods
;==========================================================

;************************************************;
;	PrintS16 ()
;		-Prints a null terminated string
;	DS=>SI: 0 terminated string
;************************************************;
 
bits	16
 
PrintS16:
		pusha				; save registers
.PrintS16Loop:
		lodsb				; load next byte from string from SI to AL
		or	al, al			; Does AL=0?
		jz	PrintS16Done		; Yep, null terminator found-bail out
		mov	ah, 0eh			; Nope-Print the character
		int	10h			    ; invoke BIOS
		jmp	.PrintS16Loop	; Repeat until null terminator found
PrintS16Done:
		popa				; restore registers
		ret				    ; we are done, so return

; wait for input buffer to be clear

WaitForKeyboardControllerInput:
        in      al,0x64
        test    al,2
        jnz     WaitForKeyboardControllerInput
        ret

	; wait for output buffer to be clear

WaitForKeyboardControllerOutput:
        in      al,0x64
        test    al,1
        jz      WaitForKeyboardControllerOutput
        ret
 
;==========================================================
;
;	 32 bit Protected Mode Methods
;==========================================================

bits 32

%define		VIDMEM	0xB8000			; video memory
%define		COLS	80				; width and height of screen
%define		LINES	25
%define		CHAR_ATTRIB 63			; character attribute (White text on light blue background)

_CurX db 0							; current x/y location
_CurY db 0

;*******************************************;
;	PrintCh32								;
;		- Prints a character to the screen.	;
;	BL => Character to print				;
;*******************************************;

PrintCh32:

	pusha					;save all registers
	mov edi, VIDMEM			;get pointer to vidoe memory

	;------------------------;
	; Get current cursor pos ;
	;------------------------;

	xor eax, eax

	;-----------------------------------------------;
	;current pos = x + y * COLS						;
	;COLS is the number of characters, not bytes	;
	;-----------------------------------------------;
	
	mov ecx, COLS*2 			; *2 because char not byte
	mov al, byte [_CurY]		; store current y position in al
	mul ecx						; eax = ecx * eax(al)
	push eax 					; save the resulting y cursor position.

	;-----------------------------------------------;
	; Now we need to work out the cursors x poition ;
	;-----------------------------------------------;

	mov al, byte [_CurX] 	;Multiply _CurX by 2 to convert from byte to char
	mov cl, 2
	mul cl
	pop ecx					; restore y*COLS into ecx
	add eax, ecx			; eax + ecx = x + (y * COLS)

	;-----------------------------------------------;
	; eax now contains the offset from the start of ;
	; video memory for where to draw the char.		;
	; To get the actual position we have to add eax ;
	; to the base address of video memory, 			;
	; which we stored in edi.						;
	;-----------------------------------------------;

	xor ecx, ecx 			; clear ecx
	add edi, eax			; add our curor offset to the base address of video memory.

	;-------------------------------;
	; Chekc for a new line		  	;
	;-------------------------------;

	cmp bl, 0x0A			; 0x0A is a newline char
	je .PrintCh32NewRow		; if bl was a new line jump to the next row.

	;-------------------------------;
	; Print the char				;
	;-------------------------------;

	mov dl, bl 				; move current char into dl
	mov dh, CHAR_ATTRIB		; tell vga we want to write a character
	mov word [edi], dx		; write to video display

	;-------------------------------;
	; Update next pos				;
	;-------------------------------;

	inc byte [_CurX]
	jmp .PrintCh32Done

	;-------------------------------;
	; Go the the next row			;
	;-------------------------------;

.PrintCh32NewRow:
	mov byte [_CurX], 0		; reset x position
	inc byte [_CurY]		; curr y = y + 1;

	;-------------------------------;
	; Restore registers and return	;
	;-------------------------------;

.PrintCh32Done:
	popa
	ret


;***********************************************************;
;	PrintS32 ()												;
;		- Prints a null terminated string					;
;	param EBX = address of string							;
;***********************************************************;

PrintS32:

	;---------------------------;
	; Store registers			;
	;---------------------------;

	pusha 		; save registers
	push ebx	; copy string address from ebx -> edi
	pop edi

.PrintS32Loop:

	;---------------------------;
	; Gte curr character		;
	;---------------------------;

	mov bl, byte [edi]		; get next char
	cmp bl, 0				; does bl == 0?
	je .PrintS32Done		; if yes, jump to PrintS32done

	;---------------------------;
	; Print the char			;
	;---------------------------;

	call PrintCh32 			; PrintCh32 prints the character stored in bl

	;---------------------------;
	; Go to next character		;
	;---------------------------;

	inc edi					; next byte in string
	jmp .PrintS32Loop

.PrintS32Done:

	;---------------------------;
	; Update hardware cursor	;
	;---------------------------;

	;we only do this once per string instead of once per char because direct VGA is slow.

	mov bh, byte [_CurY]
	mov bl, byte [_CurX]
	call MoveCursor		

	popa
	ret

;**************************************************;
;	MoveCursor ()
;		- Update hardware cursor
;	parm/ bh = Y pos
;	parm/ bl = x pos
;**************************************************;

bits 32

MoveCursor:

	pusha				; save registers (aren't you getting tired of this comment?)

	;-------------------------------;
	;   Get current position        ;
	;-------------------------------;

	; Here, _CurX and _CurY are relitave to the current position on screen, not in memory.
	; That is, we don't need to worry about the byte alignment we do when displaying characters,
	; so just follow the forumla: location = _CurX + _CurY * COLS

	xor	eax, eax
	mov	ecx, COLS
	mov	al, bh			; get y pos
	mul	ecx			; multiply y*COLS
	add	al, bl			; Now add x
	mov	ebx, eax

	;--------------------------------------;
	;   Set low byte index to VGA register ;
	;--------------------------------------;

	mov	al, 0x0f
	mov	dx, 0x03D4
	out	dx, al

	mov	al, bl
	mov	dx, 0x03D5
	out	dx, al			; low byte

	;---------------------------------------;
	;   Set high byte index to VGA register ;
	;---------------------------------------;

	xor	eax, eax

	mov	al, 0x0e
	mov	dx, 0x03D4
	out	dx, al

	mov	al, bh
	mov	dx, 0x03D5
	out	dx, al			; high byte

	popa
	ret

;**************************************************;
;	ClearScreen32 ()
;		- Clears screen
;**************************************************;

bits 32

ClearScreen32:

	pusha
	cld
	mov	edi, VIDMEM
	mov	cx, 2000
	mov	ah, CHAR_ATTRIB
	mov	al, ' '	
	rep	stosw

	mov	byte [_CurX], 0
	mov	byte [_CurY], 0
	popa
	ret

;**************************************************;
;	SetCursorPos ()
;		- Set current X/Y location
;	parm\	AL=X position
;	parm\	AH=Y position
;**************************************************;

bits 32

SetCursorPos:
	pusha
	mov	[_CurX], al		; just set the current position
	mov	[_CurY], ah
	popa
	ret

%endif ;__STDIO_INC__