%ifndef __FLOPPY_16_INC__
%define __FLOPPY_16_INC__

%include "Stdio.inc" 

;*********************************************
;	BIOS Parameter Block
;*********************************************

; BPB Begins 3 bytes from start. We do a far jump, which is 3 bytes in size.
; If you use a short jump, add a "nop" after it to offset the 3rd byte.

bpbOEM			     DB "My OS   "			; OEM identifier (Cannot exceed 8 bytes!)
bpbBytesPerSector:  	DW 512
bpbSectorsPerCluster: 	DB 1
bpbReservedSectors: 	DW 1
bpbNumberOfFATs: 	     DB 2
bpbRootEntries: 	     DW 224
bpbTotalSectors: 	     DW 2880
bpbMedia: 		     DB 0xf8
bpbSectorsPerFAT: 	     DW 9
bpbSectorsPerTrack: 	DW 18
bpbHeadsPerCylinder: 	DW 2
bpbHiddenSectors: 	     DD 0
bpbTotalSectorsBig:      DD 0
bsDriveNumber: 	     DB 0
bsUnused: 		     DB 0
bsExtBootSignature: 	DB 0x29
bsSerialNumber:	     DD 0xa0a1a2a3
bsVolumeLabel: 	     DB "MOS FLOPPY "
bsFileSystem: 	          DB "FAT12   "

datasector  dw 0x0000
cluster     dw 0x0000

absoluteSector db 0x00
absoluteHead   db 0x00
absoluteTrack  db 0x00

;************************************************;
; Convert Logical Block Addressing (LBA) to Cylinder/Head/Sector (CHS) addressing
; AX=>LBA Address to convert
;
; absolute sector = (logical sector % sectors per track) + 1
; absolute head   = (logical sector / sectors per track) % number of heads
; absolute track  = logical sector / (sectors per track * number of heads)
;
;************************************************;

ConvertLBAToCHS:
        xor       dx, dx                              ; clear dx (xor sets the left value to 1 if the right value is different, since dx == dx it will set dx to 0)
        div       WORD [bpbSectorsPerTrack]           ; ax / WORD [bpbSectorsPerTrack]. because the divisor is a word the quotiant will be stored in ax with the remainder in dx
        inc       dl                                  ; add 1 to the remainder (why is this dl instead of dx, does that even change the behaiviour?)
        mov       BYTE [absoluteSector], dl           ; absoluteSector = mod (remainder) of above equation + 1
        xor       dx, dx                              ; clear dx
        div       WORD [bpbHeadsPerCylinder]          ; (logical sector / sectors per track) / WORD [bpbHeadsPerCylinder]  
        mov       BYTE [absoluteHead], dl             ; absoluteHead = mod of above equation
        mov       BYTE [absoluteTrack], al            ; absoluteTrack = quotiant of above equation (x / y / z == x / (y * z))
        ret

;************************************************;
; Convert CHS to LBA
; LBA = (cluster - 2) * sectors per cluster
;************************************************;

ConvertCHSToLBA:
        sub       ax, 0x0002                          ; subract 2 from cluster number
        xor       cx, cx                              ; clear cx
        mov       cl, BYTE [bpbSectorsPerCluster]     ; convert byte to word (place bpbSectorsPerCluster in lower half of cx register)
        mul       cx                                  ; multiply ax with cx, because cx is 16 bit the result will be stored in dx (left most) and ax (right most)
        add       ax, WORD [datasector]               ; offset by base data sector.
        ret

;************************************************;
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:BX=>Buffer to read to
;************************************************;

ReadSectors:
     .MAIN:
          mov       di, 0x0005                          ; set di to 5, we will decrement this every time a read fails.
     .SECTORLOOP:
          push      ax                                  ; push registers so that we can restore their current value with pop later.  
          push      bx
          push      cx
          call      ConvertLBAToCHS                     ; convert starting sector to CHS
          mov       ah, 0x02                            ; moving 0x02 into ah tells the bios that we want to read sectors when performing a 0x13 interupt.
          mov       al, 0x01                            ; during a 0x13 interupt al is the number of sectors to read.
          mov       ch, BYTE [absoluteTrack]            ; store the track in ch register 
          mov       cl, BYTE [absoluteSector]           ; store the sector in cl register 
          mov       dh, BYTE [absoluteHead]             ; store the head in dh register 
          mov       dl, BYTE [bsDriveNumber]            ; store the drive number in dl register 
          int       0x13                                ; 0x13 BIOS interupt.
          jnc       .SUCCESS                            ; if the carry flag is not set, jump to .SUCCESS, the carry flag gets set by int 0x13 if there is an error
          xor       ax, ax                              ; BIOS reset disk
          int       0x13                                ; 0x13 BIOS interupt.
          dec       di                                  ; decrement error counter, if di == 0 then the zero flag will be set, otherwise it will be cleared.
          pop       cx                                  ; restore registers to previous values.
          pop       bx
          pop       ax
          jnz       .SECTORLOOP                         ; if the zero flag is not set, jump back to .SECTORLOOP
          int       0x18                                ; BIOS interupt 0x18. throw an error that there is no bootable disk, apparently this is inconsistent accross processors?
     .SUCCESS:
          mov       si, msgProgress                     ; move the progress message into si register
          call      PrintS16                            ; call our print method
          pop       cx                                  ; restore cx, bx, ax to their previous values.
          pop       bx
          pop       ax
          add       bx, WORD [bpbBytesPerSector]        ; add bpbBytesPerSector to bx. bx is the buffer address.
          inc       ax                                  ; queue next sector (div uses the value stored in ax, see ConvertLBAToCHS: for where this is used.)
          loop      .MAIN                               ; read next sector
          ret

msgProgress db ".", 0x00

%endif ;__FLOPPY_16_INC__