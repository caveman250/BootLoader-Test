;*********************************************
;	Stage2.asm
;		- Second Stage Bootloader
;
;	Operating Systems Development Series
;*********************************************

org 0x500				; we are loaded at 0x500, just above the bios

bits 16					; we are still in real mode

; we are loaded at linear address 0x10000

jmp main				; jump to main

%include "Stdio.inc"			; basic i/o methods
%include "Gdt.inc"				; Gdt methods
%include "Fat12.inc"
%include "Common.inc"

;*************************************************;
;	Data Section
;************************************************;

MsgPreparingToLoad	db	"Preparing to load operating system...",13,10,0
MsgLoadingA20 db "Enabling A20 address line...",13,10,0
MsgInstallingGDT db "Installing GDT...",13,10,0
MsgDone db "Done.",13,10,0
MsgFailure db "ERROR: FAILED TO FIND KRNL.SYS",13,10,0

;*************************************************;
;	Second Stage Loader Entry Point
;************************************************;

main:
	;-------------------------------;
	;   Setup segments and stack	;
	;-------------------------------;
	cli						; clear interrupts
	xor ax, ax 				;clear registers
	mov ds, ax
	mov es, ax
	mov ax, 0x9000			;stack is from 0x9000 to 0xffff
	mov ss, ax
	mov sp, 0xFFFF
	sti 					; enable interupts

	;-------------------------------;
	;   Print loading message	;
	;-------------------------------;

	mov si, MsgPreparingToLoad
	call PrintS16

	;-------------------------------;
	;   Install our GDT		;
	;-------------------------------;
	mov si, MsgInstallingGDT
	call PrintS16
	
	call	InstallGDT		; install GDT

	mov si, MsgDone
	call PrintS16


	;-------------------------------;
	;   Enable A20
	; 	through the keyboard controller out
	;-------------------------------;
	mov si, MsgLoadingA20
	call PrintS16

	cli
	pusha

	call    WaitForKeyboardControllerInput
	mov     al,0xAD
	out     0x64,al		; disable keyboard
	call    WaitForKeyboardControllerInput

	mov     al,0xD0
	out 	0x64, al
	call    WaitForKeyboardControllerOutput

	in		al, 0x60
	push 	eax
	call	WaitForKeyboardControllerInput

	mov		al, 0xD1
	out 	0x64, al
	call 	WaitForKeyboardControllerInput

	pop     eax
	or      al,2		; set bit 1 (enable a20)
	out     0x60,al		; write out data back to the output port

	call    WaitForKeyboardControllerInput
	mov     al,0xAE		; enable keyboard
	out     0x64,al

	call    WaitForKeyboardControllerInput
	popa
	sti

	mov si, MsgDone
	call PrintS16

	;-------------------------------;
	; Initialize filesystem		;
	;-------------------------------;

	call	LoadRoot		; Load root directory table

	;-------------------------------;
	; Load Kernel			;
	;-------------------------------;

	mov	ebx, 0			; BX:BP points to buffer to load to
	mov	bp, IMAGE_RMODE_BASE
	mov	si, ImageName		; our file to load
	call	LoadFile		; load our file
	mov	dword [ImageSize], ecx	; save size of kernel
	cmp	ax, 0			; Test for success
	je	EnterStage3		; yep--onto Stage 3!
	mov	si, MsgFailure		; Nope--print error
	call	PrintS16
	mov	ah, 0
	int     0x16                    ; await keypress
	int     0x19                    ; warm boot computer
	cli				; If we get here, something really went wong
	hlt

	;-------------------------------;
	;   Go into protected mode		;
	;-------------------------------;
EnterStage3:
	cli					; clear interrupts
	mov	eax, cr0		; set bit 0 in cr0--enter pmode
	or	eax, 1
	mov	cr0, eax

	jmp	CODE_DESC:Stage3

;******************************************************
;	Stage 3 entry point
;******************************************************

bits 32

Stage3:

	;-------------------------------;
	;   Set registers				;
	;-------------------------------;

	mov		ax, DATA_DESC		; set data segments to data selector (0x10)
	mov		ds, ax
	mov		ss, ax
	mov		es, ax
	mov		esp, 90000h		; stack begins from 90000h

CopyImage:
  	 mov	eax, dword [ImageSize]
  	 movzx	ebx, word [bpbBytesPerSector]
  	 mul	ebx
  	 mov	ebx, 4
  	 div	ebx
   	 cld
   	 mov    esi, IMAGE_RMODE_BASE
   	 mov	edi, IMAGE_PMODE_BASE
   	 mov	ecx, eax
   	 rep	movsd                   ; copy image to its protected mode address

	;---------------------------------------;
	;   Execute Kernel			;
	;---------------------------------------;

	jmp	CODE_DESC:IMAGE_PMODE_BASE; jump to our kernel! Note: This assumes Kernel's entry point is at 1 MB

	;---------------------------------------;
	;   Stop execution			;
	;---------------------------------------;

	cli
	hlt


